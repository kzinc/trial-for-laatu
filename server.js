const express = require('express');
const bodyParser = require('body-parser');

const app = express();
const port = process.env.PORT || 5000;

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));

const Dataset = {
  1: {
    name: 'Waymar',
    whois: 'Parrot',
  },
  2: {
    name: 'Gared',
    whois: 'Cat',
  },
};

app.get('/api/dataset', (req, res) => {
  res.send(JSON.stringify(Dataset));
});

app.post('/api/getPet', ({ body }, res) => {
  const { petId } = body.post;
  const result = Dataset[petId] ? { ...Dataset[petId], petId } : { id: '', name: '', whois: '' };
  res.send(result);
});

app.post('/api/editDatasetRecord', ({ body }, res) => {
  const { id, newValues } = body.post;
  Object.keys(newValues).foreach((field) => {
    Dataset[id][field] = newValues[field];
  });
  res.send(Dataset[id]);
});

app.post('/api/deleteRecord', ({ body }, res) => {
  const { post } = body;
  delete (Dataset[post]);
  res.send(JSON.stringify(true));
});

app.listen(port, () => console.log(`Listening on port ${port}`));
