import { combineReducers } from 'redux';
import { ActionsEnum, ActionCheckbox } from '../actions/actions';

export const initialState = {
  checkbox:
  [
    { checked: true },
    { checked: false },
    { checked: false },
  ],
};

export interface State {
  checkbox: Checkbox[];
}

export interface Checkbox {
  checked: boolean;
}

const checkbox = (state: Checkbox[] = [], action: ActionCheckbox): Checkbox[] => {
  switch (action.type) {
    case ActionsEnum.TOGGLE_CHECKBOX:
      return state.map((item, index) => {
        if (index === action.index) {
          return { checked: !item.checked };
        }
        return item;
      });
    default:
      return state;
  }
};

const sampleApp = combineReducers({
  checkbox,
});

export default sampleApp;
