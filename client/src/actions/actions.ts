export enum ActionsEnum {
  TOGGLE_CHECKBOX = 'TOGGLE_CHECKBOX',
}

interface Action{
  type: ActionsEnum;
  [key: string]: any;
}

export interface ActionCheckbox extends Action{
  index: number;
}

export const toggleCheckbox = (index: number): ActionCheckbox => ({
  type: ActionsEnum.TOGGLE_CHECKBOX, index,
});
