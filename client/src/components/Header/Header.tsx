import React, { ReactElement } from 'react';
import { Link } from 'react-router-dom';

const Header = (): ReactElement => (
  <div>
    <div>
      <ul>
        <li><Link to="/">Home</Link></li>
        <li><Link to="/pets">Pets</Link></li>
        <li><Link to="/info">Info</Link></li>
      </ul>
    </div>
  </div>
);
export default Header;
