/* eslint-disable jsx-a11y/label-has-associated-control */
import React, { ReactElement } from 'react';
import { connect } from 'react-redux';
import { Checkbox } from '../../reducers/reducers';
import { ActionCheckbox, toggleCheckbox } from '../../actions/actions';

const mapStateToProps = (state: {checkbox: Checkbox[]}): {checkbox: Checkbox[]} => state;

const mapDispatchToProps = (dispatch: (arg0: ActionCheckbox) => void): {
  onCheckboxClick: (id: number) => void;
} => ({
  onCheckboxClick: (id: number): void => { dispatch(toggleCheckbox(id)); },
});

interface HomeProps {
  checkbox: Checkbox[];
  onCheckboxClick: (id: number) => void;
}

const Home = connect(mapStateToProps, mapDispatchToProps)(({
  checkbox, onCheckboxClick,
}: HomeProps): ReactElement => (
  <div>
    <h1>Welcome to the Template!</h1>
    {checkbox.map((item, i) => (
      // eslint-disable-next-line react/no-array-index-key
      <label key={i}>
        {i}
        :
        <input
          type="checkbox"
          checked={item.checked}
          name={`${i}`}
          value={i}
          onChange={(e): void => onCheckboxClick(+e.target.value)}
        />
      </label>
    ))}
  </div>
));

export default Home;
