import React, { ReactElement } from 'react';
import { Switch, Route } from 'react-router-dom';
import FullPet from './FullPet';
import Pet from './Pet';

const PetContainer = (): ReactElement => (
  <Switch>
    <Route exact path="/pets" component={FullPet} />
    <Route path="/pets/:number" component={Pet} />
  </Switch>
);

export default PetContainer;
