import React, { ReactElement, useState, useEffect } from 'react';
import { Link } from 'react-router-dom';
import { getRequest } from '../helpers/RequestAPI';

interface Pet{
  name: string;
  whois: string;
}

const FullPet = (): ReactElement => {
  const [petsList, setPetsList] = useState([]);

  useEffect(() => {
    getRequest('dataset').then((newPetsList) => {
      setPetsList(newPetsList);
    });
  }, []);

  return (
    <div>
      {Object.keys(petsList).map((petId) => {
        const pet: Pet = petsList[+petId];
        return (
          <div key={petId}>
            <Link to={`/pets/${petId}`}>
              <div>{petId}</div>
              <div>{pet.name}</div>
              <div>{pet.whois}</div>
            </Link>
          </div>
        );
      })}
      {/* <ul>
        {
        PetsAPI.all().map((p) => (
          <li key={p.number}>

          </li>
        ))
      }
      </ul> */}
    </div>
  );
};

export default FullPet;
