import React, { ReactElement, useState, useEffect } from 'react';
import { Link } from 'react-router-dom';
import { postRequest } from '../helpers/RequestAPI';

interface PetProps {
  match: {
    params: {
      number: string;
    };
  };
}

const Pet = ({ match }: PetProps): ReactElement => {
  const [petId, setPetId] = useState(match.params.number);
  const [petInfo, setPetInfo] = useState({ petId: '', name: '', whois: '' });

  useEffect(() => {
    postRequest('getPet', { petId }).then((response) => {
      if (response.petId) {
        setPetId(response.petId);
        setPetInfo(response);
      } else {
        setPetId(response.petId);
      }
    });
  }, []);

  if (!petId) return <div>Sorry, but the pet was not found</div>;

  return (
    <div>
      <h1>
        #
        {petInfo.petId}
        {petInfo.name}
      </h1>
      <h2>
        Whois:
        {petInfo.whois}
      </h2>
      <Link to="/pets">Back</Link>
    </div>
  );
};


export default Pet;
