import React, { ReactElement } from 'react';
import { Route, Switch } from 'react-router-dom';
import Home from '../Home/Home';
import Pets from '../Pets/Pets';
import Info from '../Info/Info';

const Main = (): ReactElement => (
  <main>
    <Switch>
      <Route exact path="/" component={Home} />
      <Route path="/pets" component={Pets} />
      <Route path="/info" component={Info} />
    </Switch>
  </main>
);

export default Main;
