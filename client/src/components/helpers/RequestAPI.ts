interface PostInterface {
  [key: string]: any;
}

export const postRequest = async (url: string, post: PostInterface): Promise<any> => {
  const response = await fetch(`/api/${url}`, {
    method: 'POST',
    headers: {
      'Content-Type': 'application/json',
    },
    body: JSON.stringify({ post }),
  });
  const body = await response.json();
  if (response.status !== 200) throw Error(body.message);
  return body;
};

export const getRequest = async (url: string): Promise<any> => {
  const response = await fetch(`/api/${url}`);
  const body = await response.json();
  if (response.status !== 200) throw Error(body.message);
  return body;
};
