import React, { ReactElement } from 'react';
import './App.css';
import Header from './components/Header/Header';
import Main from './components/Main/Main';

const App = (): ReactElement => (
  <div>
    <Header />
    <Main />
  </div>
);

export default App;
